app.controller('homeController', function ($scope, $timeout, $state, helperFactory, httpServices, $base64, $templateCache, $http, $location) {

    /* $scope.access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNkODM2YjMzYTVjOTIwZDJhNjA4ZGI2MzFiOTQ3ZmRjNWNlMmRlZmM5NTQ4ZTE1MzVkYjcxNDE0ZmE2YzEzYTY0OGEyM2U0MGUxZWRlZDU3In0.eyJhdWQiOiI0IiwianRpIjoiM2Q4MzZiMzNhNWM5MjBkMmE2MDhkYjYzMWI5NDdmZGM1Y2UyZGVmYzk1NDhlMTUzNWRiNzE0MTRmYTZjMTNhNjQ4YTIzZTQwZTFlZGVkNTciLCJpYXQiOjE1MjU2NjY4MDUsIm5iZiI6MTUyNTY2NjgwNSwiZXhwIjoxNTU3MjAyODA1LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.EQu-V84y20pIb8fcIJAch9f35S_ybJdtKx2WTYo8O86excT9VObY0MTD51AoVGMkKZYcsY2BU6mk_m4q4PYPyrkVYu5oHQua_9K3zqMmrzcz5X_w5EDyOkKrx3Ilp3WrySQc5o5Z5lxfxwjmS59k36O9RQMROk5trxN7zeA-Gr9PEh7d2_rpBP5gQ4YsRacmbrCxwe0hCepy-8zqzv_okWg_Ag--l275SjN0XtPDQEmNDR-XHiUx-eQroF-0smNdykaovWd9rHJn-IrPGkI0I3_E1FWpwDzJlIepUKXPI8j-cqSBs11DiAdC-NRpXbxW0DAoxM1Zvmilg3wbch3fJh0J-h5Nb_ieri9xNfH30bOfuoahdAVFBv8MkUle3foGagP-Y0d_4Io4oIviRiwiIdFUu2NxbaB1WNUxmS0-SyTGGsiLkd7vsEmbDKObSD_BRZDZLrXtYBJ0vIOfvpND-DPovEyMZ4m5diZ1LowaTQ1jhC35PxtPwx2ImL50Waei0JNKYOfxQ2kL9-2qQzx3xlGy5A-q4EzwkQrrK9ar-hveFP09nnJ4rPxIy2nkpCFQEsSU_z8OCJmSL8vXIeE2FUjHHnJnHcdPBWC7OcWK8uR7jg99juL8ftYVc8-Lo3Kjkm3FQnQLHg8tDmHDrD8AxnDjsEz8xuRhM7QhMt0MfTs";
    sessionStorage.setItem('accessToken', $scope.access_token); */
    $scope.formData = {};
    var segments = [];
    var sources = [];
    var segmentsObj = {
        Origin: "",
        Destination: "",
        FlightCabinClass: "",
        PreferredDepartureTime: "",
        PreferredArrivalTime: ""
    };
    var searchParams = {
        AdultCount: "1",
        ChildCount: "0",
        InfantCount: "0",
        DirectFlight: "false",
        OneStopFlight: "false",
        JourneyType: "",
        PreferredAirlines: null,
        Segments: "",
        Sources: sources
    };
    $scope.minDate = new Date().toDateString();
    
    $scope.journeyType = ['One Way', 'Round Trip', 'Multi-City'];
    $scope.currentDate = new Date();
    $scope.formData.journeyTypeVal = ($scope.journeyType.indexOf('One Way') + 1).toString();
    helperFactory.initTravellers($scope);
    $scope.selectTravellers = function (e, index, opt, val) {
        helperFactory.selectTravellers(e, index, opt, val, $scope);
    };
    $scope.airportList = {};
    $scope.showDropdown = {}
    
    $scope.selectFlightCabin = function (index) {
        $scope.flightCabinSel = index;
        //$scope.flightCabinClass = $scope.flightCabin.indexOf($scope.flightCabin[index])+1;
        console.log('cel', $scope.flightCabinSel);
    }

    /*datepicker option*/
    $scope.isOpen_depart = false;
    $scope.isOpen_return = false;

    $scope.openCalendar = function (e, cal) {
        if (cal == 'depart') {
            $scope.isOpen_depart = !$scope.isOpen_depart;
        } else {
            $scope.isOpen_return = !$scope.isOpen_return;
        }
    };
    $scope.closeCalendar = function (e, cal) {
        if (cal == 'depart') {
            $scope.isOpen_depart = false;
        } else {
            $scope.isOpen_return = false;
        }
    };

    $scope.datepickerOptions_depart = {
        showWeeks: false
    }
    $scope.datepickerOptions_return = {
        showWeeks: false
    }

    $scope.searchAirport = function (isDestination = false) {
        $scope.airportList = {};
        $scope.access_token = sessionStorage.getItem('accessToken');
        if ($scope.access_token != null ) {
            if (isDestination) {
                $scope.getAirportList($scope.formData.destination, true);
            } else {
                $scope.getAirportList($scope.formData.origin);
            }
        } else {
            $scope.getAccessToken();
        }
    };

    /** Get country list */
    $scope.getAirportList = function (keyword, isDestination = false) {
        try {
            var _data = {
                'query': keyword
            }
            if (isDestination) {
                _data.except = $scope.formData.origin
            }
            var url = 'api/getLocation';
            //var _accessToken = sessionStorage.getItem('accessToken');
            httpServices.getRequestWithParams(helperFactory.getUrl(url), _data, $scope.access_token).then(function (response) {
                if (isDestination) {
                    $scope.airportList.destination = response.data;
                } else {
                    $scope.airportList.origin = response.data;
                }
                $scope.showDropdown = {};
                if (isDestination) {
                    $scope.showDropdown.destination = true;
                } else {
                    $scope.showDropdown.origin = true;
                }

            }).catch(function (error) {
                console.log(error)
            })
        } catch (e) {
            console.error('hello ', e)
        }
    }


    $scope.selectAirport = function (object, isDestination) {
        if (isDestination) {
            if ($scope.formData.origin == object.iata_code) {
                return false;
            }
            $scope.formData.destination = object.iata_code;
        } else {
            if ($scope.formData.destination == object.iata_code) {
                return false;
            }
            $scope.formData.origin = object.iata_code;
        }
        $scope.showDropdown = {};
    }
    

    /**Get Access Token */
    $scope.getAccessToken = function () {
        var url = 'api/getAccessToken';
        httpServices.getRequest(helperFactory.getUrl(url)).then(function (response) {
            if (response.status == 1) {
                $scope.access_token = response.access_token;
                sessionStorage.setItem('accessToken', $scope.access_token);
                //$scope.getFlightDetails();
                $scope.searchAirport();
            }
        }).catch(function (error) {
            console.log(error)
        })
    }

    /**Search flight Api */
    $scope.getFlightDetails = function () {
        $state.go('search');
    }

    /**Prepare search params & validate */
    $scope.prepareData = function () {
        var response = {};
        segments = [];

        searchParams.AdultCount = $scope.totalPassangers.adult; /* ($scope.adultsSel != null) ? ($scope.adultsSel + 1).toString() : "0"; */
        searchParams.ChildCount = $scope.totalPassangers.children;  /* ($scope.childSel != null) ? ($scope.childSel + 1).toString() : "0"; */
        searchParams.InfantCount = $scope.totalPassangers.infants; /* ($scope.infantsSel != null) ? ($scope.infantsSel + 1).toString() : "0"; */
        searchParams.JourneyType = $scope.formData.journeyTypeVal;

        segmentsObj.Origin = $scope.formData.origin;
        segmentsObj.Destination = $scope.formData.destination;
        segmentsObj.FlightCabinClass = parseInt($scope.flightCabinSel) + 2;
        segmentsObj.PreferredDepartureTime = $scope.formData.startDate;
        segmentsObj.PreferredArrivalTime = $scope.formData.startDate;
        segments.push(segmentsObj);
        if ($scope.formData.journeyTypeVal == "2") { //Round Trip
            segmentsObj = {};
            segmentsObj.Origin = $scope.formData.destination;
            segmentsObj.Destination = $scope.formData.origin;
            segmentsObj.FlightCabinClass = parseInt($scope.flightCabinSel) + 2;
            segmentsObj.PreferredDepartureTime = $scope.formData.endDate;
            segmentsObj.PreferredArrivalTime = $scope.formData.endDate;
            segments.push(segmentsObj);
        }

        searchParams.Segments = segments;

        response.status = true;
        response.data = searchParams;
        sessionStorage.setItem('searchParams', JSON.stringify(searchParams));
        return response;

    }


    /**Search flights */
    $scope.searchFlights = function (form) {
        if (form.$valid) {
            var validateParams = $scope.prepareData();
            if ($scope.access_token != null && validateParams.status) {
                $scope.getFlightDetails();
            } else {
                $scope.getAccessToken();
                $scope.getFlightDetails();
            }
        }
    }
    //$scope.getAccessToken();
    $scope.resetCount = function(e,obj,key){
        e.stopPropagation();
        obj[key] = 0;
        $scope.noOfPassengers = helperFactory.sum($scope.totalPassangers);
    }
});