app.controller('searchController', function ($scope, $timeout, $state, helperFactory, httpServices, $base64, $templateCache, $http, $location) {
    var segments = [];
    var sources = [];
    var segmentsObj = {
        Origin: "",
        Destination: "",
        FlightCabinClass: "",
        PreferredDepartureTime: "",
        PreferredArrivalTime: ""
    };
    var searchParams = {
        AdultCount: "1",
        ChildCount: "0",
        InfantCount: "0",
        DirectFlight: "false",
        OneStopFlight: "false",
        JourneyType: "1",
        PreferredAirlines: null,
        Segments: "",
        Sources: sources
    };
    $scope.minDate = new Date().toDateString();
    $scope.formData = {};
    $scope.currentDate = new Date();
    $scope.journeyType = ['One Way', 'Round Trip', 'Multi-City'];
    $scope.flightCabin = ['Economy', 'Premium Economy', 'Business'];
    $scope.formData.journeyTypeValue = ($scope.journeyType.indexOf('One Way') + 1).toString();
    $scope.flightCabinSel = 0;
    $scope.helperFactory = helperFactory;
    helperFactory.initTravellers($scope);

    $scope.checkJourneyType = function (index) {
        if (index == '1') {
            $scope.formData.endDate = "";
        }
        $scope.searchResult = null;
        $scope.tripChange = true;
    };
    $scope.airportList = {};
    $scope.showDropdown = {}

    var _data = JSON.parse(sessionStorage.getItem('searchParams'));

    $scope.items = {
        selectedFlight: 1,
        currencyClass: 'la-inr',
        selectRoute: ['00', '10']
    };
    $scope.flight = {};

    if (_data != 'undefined') {
        $scope.formData = {
            origin: _data.Segments[0].Origin,
            destination: _data.Segments[0].Destination,
            startDate: _data.Segments[0].PreferredDepartureTime,
            journeyTypeValue: _data.JourneyType
        }
        $scope.totalPassangers = {
            adult: _data.AdultCount,
            children: _data.ChildCount,
            infants: _data.InfantCount
        }
        if ($scope.formData.journeyTypeValue == '2') {
            $scope.formData.endDate = _data.Segments[1].PreferredArrivalTime
        }
        $scope.flightCabinSel = _data.Segments[0].FlightCabinClass - 2;
        $scope.noOfPassengers = helperFactory.sum($scope.totalPassangers);
        //console.log('journey', $scope.formData.journeyTypeValue);
    }

    $scope.access_token = null;

    $scope.selectTravellers = function (e, index, opt, val) {
        helperFactory.selectTravellers(e, index, opt, val, $scope);
    };

    $scope.selectFlightCabin = function (index) {
        $scope.flightCabinSel = index;
    }

    $scope.searchAirport = function (isDestination = false) {
        $scope.airportList = {};
        $scope.access_token = sessionStorage.getItem('accessToken');
        if ($scope.access_token != null ) {
            if (isDestination) {
                $scope.getAirportList($scope.formData.destination, true);
            } else {
                $scope.getAirportList($scope.formData.origin);
            }
        } else {
            //alert('hello');
            $scope.getAccessToken();
        }
    };

    /** Get country list */
    $scope.getAirportList = function (keyword, isDestination = false) {
        try {
            var _data = {
                'query': keyword
            }
            if (isDestination) {
                _data.except = $scope.formData.origin
            }
            var url = 'api/getLocation';
            //var _accessToken = sessionStorage.getItem('accessToken');
            httpServices.getRequestWithParams(helperFactory.getUrl(url), _data, $scope.access_token).then(function (response) {
                if (isDestination) {
                    $scope.airportList.destination = response.data;
                } else {
                    $scope.airportList.origin = response.data;
                }
                $scope.showDropdown = {};
                if (isDestination) {
                    $scope.showDropdown.destination = true;
                } else {
                    $scope.showDropdown.origin = true;
                }

            }).catch(function (error) {
                console.log(error)
            })
        } catch (e) {
            console.error('hello ', e)
        }
    }


    $scope.selectAirport = function (object, isDestination) {
        if (isDestination) {
            if ($scope.formData.origin == object.iata_code) {
                alert('no');
                return false;
            }
            $scope.formData.destination = object.iata_code;
        } else {
            if ($scope.formData.destination == object.iata_code) {
                alert('no');
                return false;
            }
            $scope.formData.origin = object.iata_code;
        }
        $scope.showDropdown = {};
    }

    $scope.originFlight = {};
    $scope.destinationFlight = {};

    /**Search flight Api */
    $scope.getFlightDetails = function () {
        $scope.tripChange = false;
        $scope.flightClass = null;
        $scope.searchResult = null;
        $scope.currentTrip = {};
        $scope.errorMsg = null;
        var _url = 'api/searchFlights';
        var _data = JSON.parse(sessionStorage.getItem('searchParams'));
        //console.log('flight detals data', $scope.formData.journeyTypeValue);
        var _accessToken = sessionStorage.getItem('accessToken');
        //console.log('flight cabin', _data)
        switch (_data.Segments[0].FlightCabinClass) {
            case 2:
                $scope.flightClass = 'Economy';
                break;
            case 3:
                $scope.flightClass = 'Premium';
                break;
            case 4:
                $scope.flightClass = 'Business'
                break;
            default:
                $scope.flightClass = 'Economy';
                break;
        }
        httpServices.postRequest(helperFactory.getUrl(_url), _data, _accessToken).then(function (response) {
            if (response.status == 200) {
                if (response.data.Error.ErrorCode != 0) {
                    $scope.errorMsg = response.data.Error.ErrorMessage;
                } else {
                    if (response.data.Results.length == 1) {
                        $scope.tripChange = false;
                        $scope.searchResult = response.data.Results[0];
                        $scope.tripTo = response.data.Destination;
                        $scope.tripFrom = response.data.Origin;
                        $scope.currentTrip = $scope.searchResult[0].Segments[0];
                        $scope.currentTrip = $scope.currentTrip[0];
                        //console.log($scope.currentTrip);
                        //$state.go('search');
                    } else {
                        $scope.tripChange = false;
                        $scope.searchResult = response.data.Results;
                        $scope.tripTo = response.data.Destination;
                        $scope.tripFrom = response.data.Origin;
                        $scope.originFlight = $scope.searchResult[0][0];
                        $scope.destinationFlight = $scope.searchResult[1][0];
                        /* console.log('originFlight', $scope.originFlight)
                        console.log('destinationFlight', $scope.destinationFlight) */
                        $scope.currentTrip = $scope.searchResult[0][0].Segments[0];
                        $scope.currentTrip = $scope.currentTrip[0];
                        //console.log($scope.currentTrip);
                        //$state.go('search');

                    }

                }
            }
            else {
                $scope.errorMsg = response.message;
            }
        }).catch(function (error) {
            console.log(error)
        })
    }
    $scope.currentFlight = {};
    $scope.tempdata = {};
    $scope.originDateTime = {};
    $scope.destinationdateTime = {};
    $scope.setCurrentFlight = function (object) {
        $scope.currentFlight = object;
    }
    $scope.flightChange = function (obj, key) {
        if (key == 0) {
            $scope.originFlight = obj;
        } else if (key == 1) {
            $scope.destinationFlight = obj;
        }
    }
    /**Prepare search params & validate */
    $scope.prepareData = function () {
        var response = {};
        segments = [];

        searchParams.AdultCount = $scope.totalPassangers.adult; /* ($scope.adultsSel != null) ? ($scope.adultsSel + 1).toString() : "0"; */
        searchParams.ChildCount = $scope.totalPassangers.children;  /* ($scope.childSel != null) ? ($scope.childSel + 1).toString() : "0"; */
        searchParams.InfantCount = $scope.totalPassangers.infants; /* ($scope.infantsSel != null) ? ($scope.infantsSel + 1).toString() : "0"; */
        //console.log('prepare data', $scope.formData.journeyTypeValue);
        searchParams.JourneyType = $scope.formData.journeyTypeValue;

        segmentsObj.Origin = $scope.formData.origin;
        segmentsObj.Destination = $scope.formData.destination;
        segmentsObj.FlightCabinClass = parseInt($scope.flightCabinSel) + 2;
        segmentsObj.PreferredDepartureTime = $scope.formData.startDate;
        segmentsObj.PreferredArrivalTime = $scope.formData.startDate;
        segments.push(segmentsObj);
        if ($scope.formData.journeyTypeValue == "2") { //Round Trip
            segmentsObj = {};
            segmentsObj.Origin = $scope.formData.destination;
            segmentsObj.Destination = $scope.formData.origin;
            segmentsObj.FlightCabinClass = parseInt($scope.flightCabinSel) + 2;
            segmentsObj.PreferredDepartureTime = $scope.formData.endDate;
            segmentsObj.PreferredArrivalTime = $scope.formData.endDate;
            segments.push(segmentsObj);
        }

        searchParams.Segments = segments;
        response.status = true;
        response.data = searchParams;
        sessionStorage.setItem('searchParams', JSON.stringify(searchParams));
        return response;

    }

    $scope.getFlightDetails();
    /**Search flights */
    $scope.searchFlights = function (form) {
        //console.log('JourneyType', $scope.formData.journeyTypeValue);
        $scope.tripChange = true;
        if (form.$valid) {
            var validateParams = $scope.prepareData();
            if ($scope.access_token != null && validateParams.status) {
                $scope.getFlightDetails();
            } else {
                $scope.getAccessToken();
                $scope.getFlightDetails();
            }
        }
    }

    /**Get Access Token */
    $scope.getAccessToken = function () {
        var url = 'api/getAccessToken';
        httpServices.getRequest(helperFactory.getUrl(url)).then(function (response) {
            if (response.status == 1) {
                $scope.access_token = response.access_token;
                sessionStorage.setItem('accessToken', $scope.access_token);
                //$scope.getFlightDetails();
            }
        }).catch(function (error) {
            console.log(error)
        })
    }

    /* calculate price */
    $scope.renderCalculatedPrice = function (object) {
        if (typeof object.FareBreakdown !== 'undefined') {
            var priceObject = object.FareBreakdown[0];

            switch (priceObject.Currency) {
                case 'INR':
                    $scope.items.currencyClass = 'la-inr';
                    break;

            }

            return priceObject.AdditionalTxnFeeOfrd + priceObject.AdditionalTxnFeePub + priceObject.BaseFare + priceObject.PGCharge + priceObject.Tax + priceObject.YQTax;
        }
    }

    /* calculate price */
    $scope.renderCalculated = function (object) {
        if (typeof object !== 'undefined') {
            switch (object.Currency) {
                case 'INR':
                    $scope.items.currencyClass = 'la-inr';
                    break;

            }
            return object.AdditionalTxnFeeOfrd + object.AdditionalTxnFeePub + object.BaseFare + object.PGCharge + object.Tax + object.YQTax;
        }
    }
    $scope.commonDetail = {};
    $scope.fareDetails = {};
    $scope.fareDivision = function (obj1, obj2) {
        $scope.commonDetail = obj1;
        $scope.returnDetail = obj2;
        $scope.baseFare = {};
        angular.forEach(obj1, function(value, key){
            $scope.baseFare[key] = helperFactory.totalFare(obj1[key].BaseFare, obj2[key].BaseFare);
        });
        $scope.fareDetails = {
            totalBaseFare: helperFactory.totalFare(helperFactory.sumOfObject(obj1,'BaseFare'), helperFactory.sumOfObject(obj2,'BaseFare')),
            YQTax : helperFactory.totalFare(helperFactory.sumOfObject(obj1,'YQTax'), helperFactory.sumOfObject(obj2,'YQTax')),
            GST : 0,
            Tax : helperFactory.totalFare(helperFactory.sumOfObject(obj1,'Tax'), helperFactory.sumOfObject(obj2,'Tax'))
        }
        $scope.fareDetails.Total =  parseInt($scope.fareDetails.totalBaseFare) + parseInt($scope.fareDetails.YQTax) + parseInt($scope.fareDetails.Tax) + parseInt($scope.fareDetails.GST);
    }
    $scope.closeCollapse = function(){
        $('.collapse').collapse('hide');
    }
    //$scope.getAccessToken();
    $scope.resetCount = function(e,obj,key){
        e.stopPropagation();
        obj[key] = 0;
        $scope.noOfPassengers = helperFactory.sum($scope.totalPassangers);
    }
});