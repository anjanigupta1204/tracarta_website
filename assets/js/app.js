'use strict';
var app = angular.module("tracartaApp", ['tracartaApp.services', 'ngSanitize', 'tracartaApp.helperFactory', 'base64', 'ui.router', 'angularValidator', '720kb.datepicker']);

app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

  $stateProvider
    .state('home', {
      url: "/home",
      templateUrl: "templates/home.html",
      controller: 'homeController'
    })
    .state('search', {
      url: "/search",
      templateUrl: "templates/search.html",
      controller: 'searchController'
    })
  $urlRouterProvider.otherwise("/home");

  // use the HTML5 History API
  //$locationProvider.html5Mode(true);
});

app.controller('mainController', function ($scope, httpServices, helperFactory) {

});