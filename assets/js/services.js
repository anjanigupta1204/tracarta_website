angular.module('tracartaApp.services', []).service('httpServices', function ($http, $q) {
    var httpService = {};
    /**
     * @method getRequest
     * get request for API
     * @param {string} _url, {object} params
     */
    httpService.getRequest = function (_url, params = false) {
        var deferred = $q.defer();
        try {
            $http({
                method: 'GET',
                url: _url,
                headers: { 'X-Requested-With': 'XMLHttpRequest' },
                cache: false,

            }).then(function (data, status, headers, config) {
                if (data.status == 200) {
                    deferred.resolve(data.data);
                }
            }, function myError(data, status, headers, config) {
                alert('Error: ' + data.status + "\n" + data.statusText + ' !')
                deferred.reject(data.data);
            });
            return deferred.promise;
        } catch (e) {
            // console.log(e)
        }
    }
    httpService.getRequestWithParams = function (_url, _data, _accessToken) {
        var deferred = $q.defer();
        try {
            $http({
                method: 'GET',
                url: _url,
                params: _data,
                headers: { 
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'application/json', 
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + _accessToken
                },
                cache: false,
            }).then(function (data, status, headers, config) {
                if (data.status == 200) {
                    deferred.resolve(data.data);
                }
            }, function myError(data, status, headers, config) {
                alert('Error: ' + data.status + "\n" + data.statusText + ' !')
                deferred.reject(data.data);
            });
            return deferred.promise;
        } catch (e) {
            // console.log(e)
        }
    }
    httpService.postRequest = function (_url, _data, _accessToken) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: _url,
            data: _data,
            headers: {
                'Content-Type': 'application/json', 
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + _accessToken
             },
            cache: false,
        }).then(function (data, status, headers, config) {
            deferred.resolve(data.data);
        }, function myError(data, status, headers, config) {
            deferred.reject(data.data);
        });
        return deferred.promise;
    }

    httpService.postRequestWithMedia = function (_url, data) {
        var deferred = $q.defer();
        Upload.upload({
            url: _url,
            data: data,
            method: 'POST',
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
        }).then(function (response) {
            deferred.resolve(response);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    httpService.putRequest = function (_url, data, method = 'PUT') {
        var deferred = $q.defer();
        try {
            Upload.upload({
                url: _url,
                data: { _method: 'PUT', data },
                method: 'POST'
            }).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.reject(error);
            });
        } catch (e) {
            console.log(e)
        }
        return deferred.promise;
    }
    httpService.deleteRequest = function (_url, params = []) {
        var deferred = $q.defer();
        try {
            $http({
                method: 'DELETE',
                url: _url,
                params: params,
                headers: { 'X-Requested-With': 'XMLHttpRequest' },
                cache: false,

            }).then(function (data, status, headers, config) {
                if (data.status == 200) {
                    deferred.resolve(data.data);
                }
            }, function myError(data, status, headers, config) {
                alert('Error: ' + data.status + "\n" + data.statusText + ' !')
                deferred.reject(data.data);
            });
            return deferred.promise;
        } catch (e) {
            // console.log(e)
        }
    }


    return httpService;
})