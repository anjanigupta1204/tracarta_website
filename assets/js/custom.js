  $(document).ready(function () {
		
				$('body').bootstrapMaterialDesign(); 
				
			//* svg imag inline svg
			  $('img.svgicon').each(function() {
				var $img = $(this);
				var imgURL = $img.attr('src');

				$.get(imgURL, function(data) {
				  // Get the SVG tag, ignore the rest
				  var $svg = $(data).find('svg');
				  // Replace image with new SVG
				  $img.replaceWith($svg);
				});
			  });
        $(window).scroll(function() {
            if ($(document).scrollTop() >= 100) {
                $('.top-section').addClass('shrink');
            } else {
                $('.top-section').removeClass('shrink');
            }			  
        });
				//$('#myCarousel').carousel();
				$('.collapseHide').on('click',function(){
						$('.collapse').collapse('toggle');
				});
    });