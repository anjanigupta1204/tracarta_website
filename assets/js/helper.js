angular.module('tracartaApp.helperFactory', [])
    .factory('helperFactory', function (httpServices, $base64, $location) {
        var helper = {};
        /**
         * @method return site url
         * @param {string} path 
         */
        helper.getUrl = function (path = false) {
            var _url = BASE_URL + '/';

            if (path) {
                _url += path;
            }
            return _url;
        }
        helper.fieldExists = function (val, arr) {
            var found = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].field == val) {
                    found = true;
                    break;
                }
            }
            return found;
        }
        helper.replaceFieldValue = function (key, val, arr) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].field == key) {
                    arr[i].value = val
                }
            }
            return arr;
        }

        helper.orderExists = function (val, arr) {
            var found = false;
            for (var i = 0; i < arr.length; i++) {
                if (typeof arr[i].order != 'undefined') {
                    found = true;
                    break;
                }
            }
            return found;
        }
        helper.replaceOrderValue = function (val, arr, dir) {
            for (var i = 0; i < arr.length; i++) {
                if (typeof arr[i].order != 'undefined') {
                    arr[i].order = val;
                    arr[i].dir = dir;
                    break;
                }
            }
            return arr;
        }
        helper.getParameterByName = function (name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
        /**
         * @method check not empty object
         */
        helper.isEmpty = function (obj) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }
        /**
         * @method Get Image upload error messages messages
         */
        helper.getImageMessages = function (file, ht = false, wd = false, sz = false) {
            var height = (ht) ? ht : 1200;
            var width = (wd) ? wd : 1600;
            var size = (sz) ? sz : 2;
            var sizeInKb = size * 1024 * 1024;
            if (file.type == 'image/jpeg' || file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/gif') {
                if (file.size > sizeInKb) {
                    console.log('Image should not be more than ' + size + 'MB');
                    return false;
                } else if (file.$ngfHeight && file.$ngfHeight > height) {
                    console.log('Image Height should be less than ' + height + 'px');
                    return false;
                } else if (file.$ngfWidth && file.$ngfWidth > width) {
                    console.log('Image width should be less than ' + width + 'px');
                    return false;
                } else {
                    return true;
                }
            } else {
                if (!file.$ngfWidth) {
                    console.log('Only images (jpg/png/gif) are allowed');
                    return false;
                } else {
                    console.log('Only images (jpg/png/gif) are allowed');
                }

            }
        }
        /**Get request listing data */
        helper.getTabData = function (url, params = [], loading = true, scope) {
            httpServices.getRequest(url, params).then(function (data) {
                if (typeof (data.tab_content) != 'undefined') {
                    scope.tabContent = data.tab_content;
                    scope.columns = scope.tabContent.columns;
                    scope.additional_data = data.additional_data;
                    scope.values = scope.tabContent.data;
                    scope.pagination = scope.tabContent.pagination;
                    scope.showLoading = false;
                }
            }).catch(function (error) {
                console.error('Error: ', error);
            });
        }
        /**
         * @method Search Filters
         */
        helper.searchby = function (dbReference, data, $index, filterIndex = false, operator = false, scope) {
            scope.d = data;
            scope.dbReference = dbReference;
            scope.in = $index;
            if (filterIndex) {
                scope.filterIndex = filterIndex;
                scope.d = data + ';' + filterIndex;
            }
            clearTimeout(scope.typingTimer);
            if (operator) {
                scope.operator = '=';
            }
            scope.typingTimer = setTimeout(helper.doneTyping(scope), scope.doneTypingInterval);
        }
        /**
           * @method hit search data after typing completed 
           */
        helper.doneTyping = function (scope) {
            if (!helper.fieldExists(scope.dbReference, scope.globalParams)) {
                scope.globalParams.push({
                    'field': scope.dbReference,
                    'value': scope.d,
                    'operator': scope.operator
                });
            } else {
                scope.globalParams = helper.replaceFieldValue(scope.dbReference, scope.d, scope.globalParams)
            }
            scope.modelFilter.name[scope.in] = scope.d.split(";")[0];
            var url = helper.getUrl(scope.dropdown.drop);
            /* AJAX call */
            helper.getTabData(url, helper.getQueryFilters(scope), false, scope);
            scope.operator = 'LIKE';
        }
        /**
         * @method Sorting data
         */
        helper.sortBy = function (data, filterIndex = false, scope) {

            if (filterIndex) {
                data = data + ';' + filterIndex
            }
            if (!helper.orderExists(data, scope.globalParams)) {
                scope.globalParams.push({ 'order': data, 'dir': scope.dir });
            } else {
                scope.globalParams = helper.replaceOrderValue(data, scope.globalParams, scope.dir);
            }
            scope.dir = !scope.dir;
            var url = helper.getUrl(scope.dropdown.drop);
            helper.getTabData(url, helper.getQueryFilters(scope), false, scope);
        }

        /**
        * @method global query filters
        */
        helper.getQueryFilters = function (scope) {
            var filters = {};
            var order = {}
            var page = 0;
            var params = {};
            if (scope.globalParams.length) {
                var i = 0;
                var j = 0;
                angular.forEach(scope.globalParams, function (val, key) {
                    if (typeof val.field != 'undefined') {
                        filters[i] = val;
                        i++;
                    } else if (typeof val.order != 'undefined') {
                        order[j] = val;
                        j++;
                    } else if (typeof val.page != 'undefined') {
                        page = val;
                    }
                });

                params = { 'key': $base64.encode(JSON.stringify({ 'filter': filters, 'order': order, page: page })) };
                if (page > 0) {
                    params.page = page;
                }
            }
            return params;
        }

        /**
         * @method reloadTab
         */
        helper.reloadTab = function (contentClass, key = sessionStorage.activeTab, scope) {
            scope.showGrid = true;
            scope.formData = {};
            scope.messages = {};
            scope.globalParams = [];
            scope.modelFilter = [];
            scope.dropdown.drop = null;
            scope.tabContent = null;
            scope.columns = null;
            scope.additional_data = null;
            scope.values = null;
            scope.pagination = null;
            sessionStorage.currentTabDataClass = contentClass
            sessionStorage.activeTab = key;
            if (contentClass) {
                scope.dropdown.drop = contentClass
                var url = helper.getUrl(scope.dropdown.drop);
                if (scope.dropdown.drop != null) {
                    helper.getTabData(url, undefined, undefined, scope);
                }
            }

        }

        helper.initializeTabs = function (scope) {
            var curUrl = $location.absUrl().split('?')[0];
            if (typeof sessionStorage.currentUrl != 'undefined' && curUrl != sessionStorage.currentUrl) {
                sessionStorage.clear();
            }
            if (typeof sessionStorage.currentUrl == 'undefined') {
                sessionStorage.currentUrl = curUrl;
            }
            if (typeof sessionStorage.activeTab == 'undefined') {
                sessionStorage.activeTab = 0;
            }
            scope.activeTab = sessionStorage.activeTab;


            jQuery(window).load(function () {
                var tabIndex = scope.activeTab;
                var __init = jQuery('#tab-' + tabIndex + '').find('.panel-body').attr('data-tab-init');
                scope.dropdown.drop = __init;
                if (typeof sessionStorage.currentTabDataClass == 'undefined') {
                    sessionStorage.currentTabDataClass = __init
                } else {
                    scope.dropdown.drop = sessionStorage.currentTabDataClass
                }
                var url = helper.getUrl(scope.dropdown.drop);
                if (scope.dropdown.drop != null && typeof scope.dropdown.drop != 'undefined') {
                    helper.getTabData(url, undefined, undefined, scope);
                }
            })

            jQuery(document).on('click', '.pagination li a', function (e) {
                e.preventDefault();
                var url = jQuery(this).attr('href');
                var page = helper.getParameterByName('page', url);
                scope.globalParams.push({ 'page': page })
                helper.getTabData(url, scope.getQueryFilters(), undefined, scope);
                /* AJAX call */
            });
        }

        helper.timeDifference = function (startDate, endDate) {
            var startDate = startDate;
            var endDate = endDate;
            var seconds = (new Date(endDate).getTime() - new Date(startDate).getTime()) / 1000;
            var hours = Math.floor(seconds / 3600);
            var minutes = Math.floor(seconds % 3600 / 60);
            var timeDiff = hours + 'h ' + minutes + 'm';
            return timeDiff;
        }
        helper.initTravellers = function (scope) {
            scope.travellers = {
                adults: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
                children: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
                infants: ['0', '1', '2', '3']
            }
            scope.flightCabin = ['Economy', 'Premium Economy', 'Business'];
            scope.flightCabinSel = 0;
            scope.totalPassangers = {
                adult: 1,
                children: 0,
                infants: 0
            }
            scope.noOfPassengers = this.sum(scope.totalPassangers);

        }
        /**
         * 
         * @method sum of all values in an object
         */
        helper.sum = function (obj) {
            var sum = 0;
            for (var el in obj) {
                if (obj.hasOwnProperty(el)) {
                    sum += parseFloat(obj[el]);
                }
            }

            return sum;
        }

        helper.totalFare = function (val1, val2) {
            return (val1 + val2);
        }
        helper.sumOfObject = function (object, __key) {

            var totalFare = {};
            if (!this.isEmpty(object)) {
                angular.forEach(object, function (val, key) {
                    totalFare[key] = val[__key]
                });
                return this.sum(totalFare)
            }
        }
        helper.selectTravellers = function (e, index, opt, val, scope) {
            e.stopPropagation();
            switch (opt) {
                case 'adults':

                    if ((parseInt(val) + scope.noOfPassengers - scope.totalPassangers.adult) > 9 && parseInt(val) >= scope.totalPassangers.adult) {
                        return false;
                        $('.errorpopup').popover('show');
                    }
                    scope.totalPassangers.adult = parseInt(scope.travellers.adults[index]);
                    if (scope.totalPassangers.adult < scope.totalPassangers.infants) {
                        scope.totalPassangers.infants = 0;
                    }
                    break;
                case 'children':
                    if ((parseInt(val) + scope.noOfPassengers - scope.totalPassangers.children) > 9 && parseInt(val) >= scope.totalPassangers.children) {
                        return false;
                    }
                    scope.totalPassangers.children = parseInt(scope.travellers.children[index]);
                    break;
                case 'infants':
                    if ((parseInt(val) + scope.noOfPassengers - scope.totalPassangers.infants) > 9 && parseInt(val) >= scope.totalPassangers.infants) {
                        return false;
                    }
                    if (scope.totalPassangers.adult < parseInt(val)) {
                        return false;
                    }
                    scope.totalPassangers.infants = parseInt(scope.travellers.infants[index]);
                    break;
            }
            scope.noOfPassengers = this.sum(scope.totalPassangers);

        };

        return helper;
    }).filter('capitalize', function () {
        return function (input) {
            if (input != null && input.length > 1) {
                if (input.indexOf(' ') !== -1) {
                    var inputPieces,
                        i;

                    input = input.toLowerCase();
                    inputPieces = input.split(' ');

                    for (i = 0; i < inputPieces.length; i++) {
                        inputPieces[i] = capitalizeString(inputPieces[i]);
                    }

                    return inputPieces.toString().replace(/,/g, ' ');
                }
                else {
                    input = input.toLowerCase();
                    return capitalizeString(input);
                }
            } else {
                return input;
            }
            function capitalizeString(inputString) {
                return inputString.substring(0, 1).toUpperCase() + inputString.substring(1);
            }
        };
    }).filter('checkExistence', function () {
        return function (input) {
            if (input == null) {
                return 'N.A';
            } else {
                return input;
            }
        }
    })
    .filter('to_trusted', ['$sce', function ($sce) {
        return function (input) {
            if (input != null && input.length > 1) {
                return $sce.trustAsHtml(input);
            } else {
                return input;
            }

        };
    }]).filter('setIsActiveLabel', function () {
        return function (input) {
            if (input == 'is_active') {
                return 'Status';
            } else {
                return input
            }
        }
    }).filter('setStatusLabel', function () {
        return function (input, key = 'is_active') {
            if (key == 'is_active') {
                return (input > 0) ? 'Enabled' : 'Disabled'
            } else {
                return input
            }
        }
    }).filter('mediaUrl', function (helperFactory) {
        return function (input) {
            return helperFactory.getUrl('media/' + input);
        }
    }).filter('dateToISO', function () {
        return function (input) {
            return new Date(input).toISOString();
        };
    }).filter('myDateFilter', function () {
        return function (input) {
            console.log(input)
            // set minutes to seconds
            var seconds = input * 60

            // calculate (and subtract) whole days
            var days = Math.floor(seconds / 86400);
            seconds -= days * 86400;

            // calculate (and subtract) whole hours
            var hours = Math.floor(seconds / 3600) % 24;
            seconds -= hours * 3600;

            // calculate (and subtract) whole minutes
            var minutes = Math.floor(seconds / 60) % 60;
            var __days = (days) ? days + 'd ' : '';
            var __hrs = (hours) ? hours + 'h ' : '';
            var __mins = (minutes) ? minutes + 'm ' : '';
            return __days + __hrs + __mins;
        }
    });